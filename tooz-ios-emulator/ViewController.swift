import UIKit
import CoreMotion
import tooz_ios_emulator_core

class ViewController: UIViewController, ConnectionStateListener, FrameReceivedListener, SensorRequestedListener {
    
    private let logTag = "BLE:"
    private var tooz: ToozBluetooth?
    let motion = CMMotionManager()
    var accelerationTimer: Timer?
    var gyroscopeTimer: Timer?
        
    @IBOutlet var container: UIView!
    
    private let imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        return view
    } ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tooz = ToozBluetooth(connectionStateListener: self, frameListener: self, sensorRequestedListener: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if container.subviews.count == 0 {
            setupUi()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopSensor()
    }
    
    func sensorRequested(sensors: Set<SensorOfInterest>) {
        
        sensors.forEach { sensor in
            switch sensor.name {
            case .acceleration:
                
                print("\(logTag) sensor::: acceleration delay: \(sensor.delay)")
                
                if sensor.delay > 0 {
                    if motion.isAccelerometerActive != true {
                        let interval = Double(sensor.delay) / 1000.0
                        print("\(logTag) sensor::: startAccelerometer with interval: \(interval)")
                        if interval > 0.0 {
                            startAccelerometer(interval: interval)
                        }
                    }
                } else {
                    print("\(logTag) sensor::: accelerationTimer?.invalidate()")
                    accelerationTimer?.invalidate()
                    motion.stopAccelerometerUpdates()
                }
            case .gyroscope:
                print("\(logTag) sensor::: gyroscope delay: \(sensor.delay)")
                if sensor.delay > 0 {
                    if motion.isGyroActive != true {
                        let interval = Double(sensor.delay) / 1000.0
                        print("\(logTag) sensor::: gyroscope with interval: \(interval)")
                        if interval > 0.0 {
                            startGyroscope(interval: interval)
                        }
                    }
                } else {
                    gyroscopeTimer?.invalidate()
                    motion.stopGyroUpdates()
                }
            default:
                if sensor.delay > 0 {
                    print("\(logTag) sensor \(sensor.name) was requested. This sensor is currently not implemented in the emulator")
                }
            }
        }
    }
    
    func startGyroscope(interval: Double) {
        if motion.isGyroAvailable {
            motion.gyroUpdateInterval = interval
            motion.startGyroUpdates()
            gyroscopeTimer?.invalidate()
            gyroscopeTimer = Timer(fire: Date(), interval: (interval), repeats: true, block: { (gyroscopeTimer) in
                if let data = self.motion.gyroData {
                    let x = data.rotationRate.x
                    let y = data.rotationRate.y
                    let z = data.rotationRate.z
                    self.tooz?.sensorUpdated(type: SensorType.gyroscope, x: x, y: y, z: z, w: nil)
                }
            })
            RunLoop.current.add(self.accelerationTimer!, forMode: RunLoop.Mode.default)
       }
    }
    
    func startAccelerometer(interval: Double) {
        if motion.isAccelerometerAvailable {
            motion.accelerometerUpdateInterval = interval
            motion.startAccelerometerUpdates()
            accelerationTimer?.invalidate()
            accelerationTimer = Timer(fire: Date(), interval: (interval), repeats: true, block: { (accelerationTimer) in
                if let data = self.motion.accelerometerData {
                    let x = data.acceleration.x
                    let y = data.acceleration.y
                    let z = data.acceleration.z
                    self.tooz?.sensorUpdated(type: SensorType.acceleration, x: x, y: y, z: z, w: nil)
                }
            })
            RunLoop.current.add(self.accelerationTimer!, forMode: RunLoop.Mode.default)
       }
    }
    
    func onConnectionStateChanged(connectionState: ConnectionState) {
        // nt really something to do here....
    }
    
    func frameReceived(frame: Data) {
        imageView.image = UIImage(data: frame)
    }
    
    @objc func button_a_1s_Action() {
        tooz?.sendButtonMessage(code: ButtonCode.a_1s)
    }
    
    @objc func button_a_1l_Action() {
        tooz?.sendButtonMessage(code: ButtonCode.a_1l)
    }
    
    @objc func button_b_1s_Action() {
        tooz?.sendButtonMessage(code: ButtonCode.b_1s)
    }
    
    @objc func button_b_1l_Action() {
        tooz?.sendButtonMessage(code: ButtonCode.b_1l)
    }
    
    private func setupUi() {
        // Image
        
        let imageWidth = 400 / UIScreen.main.scale
        let imageHeight = 640 / UIScreen.main.scale
        let screenSize = UIScreen.main.bounds
        
        let imageXPosition = CGFloat((Int(screenSize.width) / 2)) - (imageWidth / 2)

        imageView.frame = CGRect(x: imageXPosition, y: 40, width: imageWidth, height: imageHeight)
        container.addSubview(imageView)
        
        // Buttons
        let buttonCornerRadius: CGFloat = 8
        let buttonWidth = 165
        let buttonHeight = 50
        let topMargin = Int(screenSize.height) - (4 * buttonHeight) - 75
        
        let a_1s = UIButton()
        a_1s.setTitle("Short Tap", for: UIControl.State.normal)
        a_1s.frame = CGRect(x: (Int(screenSize.width) / 2) - (buttonWidth + 10), y: Int(topMargin), width: buttonWidth, height: buttonHeight)
        a_1s.backgroundColor = UIColor.black
        a_1s.layer.cornerRadius = buttonCornerRadius
        a_1s.addTarget(self, action: #selector(button_a_1s_Action), for: .touchUpInside)
        
        let a_1l = UIButton()
        a_1l.setTitle("Forward Swipe", for: UIControl.State.normal)
        a_1l.frame = CGRect(x: (Int(screenSize.width) / 2) + 10, y: Int(topMargin), width: buttonWidth, height: buttonHeight)
        a_1l.backgroundColor = UIColor.black
        a_1l.layer.cornerRadius = buttonCornerRadius
        a_1l.addTarget(self, action: #selector(button_a_1l_Action), for: .touchUpInside)
        
        let b_1s = UIButton()
        b_1s.setTitle("Long Tap", for: UIControl.State.normal)
        b_1s.frame = CGRect(x: (Int(screenSize.width) / 2) - (buttonWidth + 10), y: Int(topMargin) + 10 + buttonHeight, width: buttonWidth, height: buttonHeight)
        b_1s.backgroundColor = UIColor.black
        b_1s.layer.cornerRadius = buttonCornerRadius
        b_1s.addTarget(self, action: #selector(button_b_1s_Action), for: .touchUpInside)
        
        let b_1l = UIButton()
        b_1l.setTitle("Backward Swipe", for: UIControl.State.normal)
        b_1l.frame = CGRect(x: (Int(screenSize.width) / 2) + 10, y: Int(topMargin) + 10 + buttonHeight, width: buttonWidth, height: buttonHeight)
        b_1l.backgroundColor = UIColor.black
        b_1l.layer.cornerRadius = buttonCornerRadius
        b_1l.addTarget(self, action: #selector(button_b_1l_Action), for: .touchUpInside)
        
        container.addSubview(a_1s)
        container.addSubview(a_1l)
        container.addSubview(b_1s)
        container.addSubview(b_1l)
        
    }
    
    private func stopSensor() {
        accelerationTimer?.invalidate()
        gyroscopeTimer?.invalidate()
        motion.stopAccelerometerUpdates()
        motion.stopGyroUpdates()
    }

}

