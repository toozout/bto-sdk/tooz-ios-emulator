# tooz iOS-Emulator Beta-Version

## 1. Introduction

The tooz iOS-Emulator emulates the main functionalities of the tooz glasses:

* **Sending images**
* **Receiving button events**
* **Receiving sensor events** (Note: The emulator only supports accelerometer and gyroscope sensors)


## 2. Getting started

Clone this repository, adapt the signing profile by changing the _Bundle Identifier_ and start the emulator app on a physical device. Use another physical device to run your app which implements the SDK. Enable bluetooth on both devices.  
**Note**: The emulator will not run in a simulator. All builds for a simulator will fail. 

## 3. Troubleshooting

If any problem should arise, feel free to contact us at <dev@tooztech.com>. 

**Note**: The emulator does not emulate all the functionality of the tooz glasses and **it is much slower** - images take longer to be received by the emulator than by the physical glasses. Therefore it is strongly advised to not release an app to production that was not tested with the physcial glasses.

